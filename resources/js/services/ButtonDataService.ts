import http from './http-common';
import { AxiosResponse } from "axios";
import { Button } from '../types/';

export default class ButtonDataService {
    static getAll(): Promise<AxiosResponse<Button[]>> {
        return http.get<Button[]>("/buttons");
    }

    static get(id: number): Promise<AxiosResponse<Button>> {
        return http.get<Button>(`/buttons/${id}`);
    }

    static delete(id: number): Promise<AxiosResponse> {
        return http.delete(`/buttons/${id}`);
    }

    static edit(id: number, button: Button): Promise<AxiosResponse<Button>> {
        return http.put(`/buttons/${id}`, button);
    }

    static store(button: Button): Promise<AxiosResponse<Button>> {
        return http.post(`/buttons/`, button);
    }
}