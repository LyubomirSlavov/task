<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateButtonTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /** @test */
    public function test_create_new_button()
    {
        $title = $this->faker->name;

        $response = $this->post(
            '/buttons',
            [
                'id' => 1,
                'title' => $title,
                'url' => $this->faker->url(),
                'color' => $this->faker->colorName(),
               
            ]
        );

        $response->assertStatus(200);

        $this->assertDatabaseHas('buttons', [
            'title' => $title,
        ]);
    }

    /** @test */
    public function test_fail_creating_button_missing_title()
    {
        $title = $this->faker->name;

        $response = $this->post(
            '/buttons',
            [
                'id' => 1,
                'url' => $this->faker->url(),
                'color' => $this->faker->colorName(),
            ]
        );

        $response->assertSessionHasErrors("title");

        $this->assertDatabaseCount('buttons', 0);
    }

    /** @test */
    public function test_fail_creating_button_missing_url()
    {
        $title = $this->faker->name;
    
        $response = $this->post(
            '/buttons',
            [
                'id' => 1,
                'title' => $title,
                'color' => $this->faker->colorName(),
            ]
        );
    
        $response->assertSessionHasErrors("url");
    
        $this->assertDatabaseMissing('buttons', [
                'title' => $title,
            ]);
    }

    /** @test */
    public function test_fail_creating_button_missing_color()
    {
        $title = $this->faker->name;

        $response = $this->post(
            '/buttons',
            [
                'id' => 1,
                'title' => $title,
                'url' => $this->faker->url(),
            ]
        );

        $response->assertSessionHasErrors("color");

        $this->assertDatabaseMissing('buttons', [
            'title' => $title,
        ]);
    }
}
