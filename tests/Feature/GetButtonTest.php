<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;
use App\Models\Button;

class GetButtonTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /** @test */
    public function test_get_one_button()
    {
        Button::factory()->count(5)->create();

        $button = Button::all()->random();

        $response = $this->get("/buttons/{$button->id}");

        $response->assertJson(
            fn (AssertableJson $json) =>
                $json->where('id', $button->id)
                    ->where('title', $button->title)
                    ->etc()
        );
    }

    /** @test */
    public function test_get_all_buttons()
    {
        Button::factory()->count(5)->create();


        $response = $this->get('/buttons');
        $button = Button::latest()->first();

        $count = Button::count();
      
        $response
            ->assertJson(
                fn (AssertableJson $json) =>
                $json->has($count)->first(
                    fn ($json) =>
                $json->where('id', $button->id)
                     ->etc()
                )
            );
    }
}
