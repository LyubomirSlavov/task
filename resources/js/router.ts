import { createWebHashHistory, createRouter, RouteRecordRaw } from "vue-router";

import HomeView from "./views/Home.vue";
import ButtonForm from "./views/ButtonForm.vue"

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        alias: "/home",
        name: "home",
        component: HomeView,
    },
    {
        path: "/buttons/:id",
        name: "buttonform",
        component: ButtonForm,
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

export default router;
