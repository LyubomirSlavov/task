<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a>
<a href="https://v3.vuejs.org/" target="_blank"><img src="https://www.kindpng.com/picc/m/452-4529223_vue-js-logo-png-transparent-png.png" width="200"></a>
<a href="https://www.typescriptlang.org/" target="_blank"><img src="https://i.pinimg.com/originals/c3/8e/e8/c38ee8475ee7f3680f706c56c3a1194c.png" width="200"></a>
</p>



## Shkolo.bg Recruitment task

### Task 1: Implement a customizable web dashboard.
* The dashboard must simply contain a grid of 9 cells (check the mockup below).
* Each cell must include a colored button. On click the button must navigate the user to
previously set hyperlink.
* In case the hyperlink is not set yet, then on button click the user must be navigated to
another page, where the user would have the opportunity to set the button’s
configuration (hyperlink + color).
* Think about EDIT/DELETE scenario (in case the user wants to remove/modify the
button’s configuration)
* The grid must be responsive (i.e. auto-resizing based on the device resolution).
* The task includes database design (SQL), backend implementation (PHP/Laravel),
frontend implementation (HTML, CSS, JavaScript).
* Please deploy the application on the cloud and send us the demo link + the source
code (e.g. git repo) to miroslav.dzhokanov@shkolo.com