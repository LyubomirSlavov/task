/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import { createApp } from "vue"
import App from "./App.vue"
import router from "./router";



const app = createApp(App);

app.use(router);

const vm = app.mount('#app')
