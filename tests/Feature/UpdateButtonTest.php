<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Button;

class UpdateButtonTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /** @test  */
    public function test_update_button()
    {
        $this->withoutExceptionHandling();

        Button::factory()->count(5)->create();

        $button = Button::all()->random();

        $title = $this->faker->name;
        $url = $this->faker->url();
        $color = $this->faker->colorName();

        $response = $this->put("/buttons/{$button->id}", [
            'title' => $title,
            'url' => $url,
            'color' => $color
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('buttons', [
            'id' => $button->id,
            'title' => $title,
            'url' => $url,
            'color' => $color
        ]);
    }

    /** @test  */
    public function test_fail_update_button_title()
    {
        Button::factory()->count(5)->create();

        $title = $this->faker->name;
        $url = $this->faker->url();
        $color = $this->faker->colorName();

        $response = $this->put('/buttons/1', [
            'url' => $url,
            'color' => $color
        ]);

        $response->assertSessionHasErrors('title');

        $this->assertDatabaseMissing('buttons', [
            'id' => 1,
            'title' => $title,
            'url' => $url,
            'color' => $color
        ]);
    }

    /** @test  */
    public function test_fail_update_button_url()
    {
        Button::factory()->count(5)->create();

        $title = $this->faker->name;
        $url = $this->faker->url();
        $color = $this->faker->colorName();

        $response = $this->put('/buttons/1', [
            'title' => $title,
            
            'color' => $color
        ]);

        $response->assertSessionHasErrors('url');

        $this->assertDatabaseMissing('buttons', [
            'id' => 1,
            'title' => $title,
            'url' => $url,
            'color' => $color
        ]);
    }

    /** @test  */
    public function test_fail_update_button_color()
    {
        Button::factory()->count(5)->create();

        $title = $this->faker->name;
        $url = $this->faker->url();
        $color = $this->faker->colorName();

        $response = $this->put('/buttons/1', [
            'title' => $title,
            'url' => $url,
        ]);

        $response->assertSessionHasErrors('color');

        $this->assertDatabaseMissing('buttons', [
            'id' => 1,
            'title' => $title,
            'url' => $url,
            'color' => $color
        ]);
    }
}
