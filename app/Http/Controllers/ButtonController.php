<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Button;
use App\Http\Requests\StoreButtonRequest;
use App\Http\Requests\UpdateButtonRequest;

class ButtonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $buttons = Button::all();

        return response()->json($buttons);
    }

    /**
     * Display a single item of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $index)
    {
        $button = Button::findOrFail($index);

        return response()->json($button);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreButtonRequest $request)
    {
        return response()->json(Button::create($request->validated()));
    }


    /**
    * Update the specified resource in storage.
    *
    * @param  Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateButtonRequest $request, $id)
    {
        $button = Button::findOrFail($id);
        $button->update($request->validated());

        return response()->json($button);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $button = Button::findOrFail($id);
     
        $button->delete();
        

        return response('');
    }
}
