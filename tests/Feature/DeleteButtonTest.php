<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Button;

class DeleteButtonTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /** @test */
    public function test_button_deletion()
    {
        Button::factory()->count(5)->create();

        $button = Button::all()->random();
        $count = Button::count();
        
        $response = $this->delete("/buttons/{$button->id}");

        $response->assertStatus(200);

        $this->assertDatabaseMissing('buttons', [
            'id' => $button->id,
        ]);

        $this->assertDatabaseCount('buttons', $count - 1);
    }

    /** @test */
    public function test_fail_button_deletion_resource_doesnt_exist()
    {
        Button::factory()->count(5)->create();
        
        $response = $this->delete('/buttons/100');
       
        $response->assertStatus(404);

        $this->assertDatabaseCount('buttons', 5);
    }
}
