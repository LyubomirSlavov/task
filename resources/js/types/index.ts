export interface Button {
    id: number,
    title: string,
    url: string,
    color: string,
}

export interface ButtonIndex {
    id: number
}